mvn package
docker build -t srini_repo:"$1" .
docker tag srini_repo:"$1" 528926686889.dkr.ecr.eu-west-1.amazonaws.com/srini_repo:"$1"
$(aws ecr get-login --no-include-email --region eu-west-1)
docker push 528926686889.dkr.ecr.eu-west-1.amazonaws.com/srini_repo:"$1"
